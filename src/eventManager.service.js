/*global angular*/
'use strict';

var EventManager = require('./eventManager');

angular.
    module('bitcraft-event-manager').
    factory('EventManager', function () {
        function create() {
            return new EventManager();
        }

        return {
            create: create
        };
    });